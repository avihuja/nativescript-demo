// import VueDevtools from 'nativescript-vue-devtools'
import Vue from 'nativescript-vue'
import App from './components/App'
import store from './store'

var SocketIO = require('nativescript-socketio').SocketIO; 
var socketIO = new SocketIO("http://192.168.1.103:3000/", {});
socketIO.connect();

// if(TNS_ENV !== 'production') {
//   Vue.use(VueDevtools)
// }
// Vue.use(VueDevtools, { host: '192.168.148.1:8098' })
  
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')


new Vue({
  store,
  render: h => h('frame', [h(App)])
}).$start()
